# How to handle database changes in a zero-downtime compliant way

This repository contains the cheatsheet and its sources mentioned in the article: https://medium.com/ekino-france/how-to-handle-database-changes-in-a-zero-downtime-compliant-way-908763264c58

## About me

See https://brambaud.github.io/
